﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CiscoAkademi.Startup))]
namespace CiscoAkademi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
